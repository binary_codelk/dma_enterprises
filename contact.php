<!DOCTYPE html>
<html lang="en">
<?php include "layout/head.php"; ?>

<body>
    <!--================Menu Area =================-->

    <?php include "layout/navigation.php"; ?>
    <!--================End Menu Area =================-->

    <!--================Categories Banner Area =================-->
    <section class="categories_banner_area">
        <div class="container">
            <div class="solid_banner_inner">
                <h3>Contact</h3>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="contact.html">Contact</a></li>
                </ul>
            </div>
        </div>
    </section>
    <!--================End Categories Banner Area =================-->

    <!--================Contact Area =================-->
    <section class="contact_area p_100">
        <div class="container">
            <div class="contact_title">
                <h2>Get in Touch</h2>
                <p>Please contact us for more details. We are looking forward to help you at any time.</p>
            </div>
            <div class="row contact_details">
                <div class="col-lg-4 col-md-6">
                    <div class="media">
                        <div class="d-flex">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                            <p>No 32, Attampitiya Road, Helamuduna, Bandarawela</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="media">
                        <div class="d-flex">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                            <a href="tel:+94715122228">+94 715 122 228</a>
                            <a href="tel:+94705290601">+94 705 290 601</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="media">
                        <div class="d-flex">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                            <a href="mailto:dissanaajith63@gmail.com">dissanaajith63@gmail.com</a>
                            <a href="mailto:support@persuit.com">dmaenterprises@gmail.com</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact_form_inner">
                <h3>Drop a Message</h3>
                <form class="contact_us_form row" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                    <div class="form-group col-lg-4">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Full Name *">
                    </div>
                    <div class="form-group col-lg-4">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email Address *">
                    </div>
                    <div class="form-group col-lg-4">
                        <input type="text" class="form-control" id="website" name="website" placeholder="Your Website">
                    </div>
                    <div class="form-group col-lg-12">
                        <textarea class="form-control" name="message" id="message" rows="1" placeholder="Type Your Message..."></textarea>
                    </div>
                    <div class="form-group col-lg-12">
                        <button type="submit" value="submit" class="btn update_btn form-control">Send Message</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="container align-items" style="margin-top: 30px;">
            <div class="mapouter">
                <div class="gmap_canvas"><iframe width="1000" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=bandarawela%20hospital&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://fmovies-online.net"></a><br>
                    <style>
                        .mapouter {
                            position: relative;
                            text-align: right;
                            height: 400px;
                            width: 1000px;
                        }
                    </style><a href="https://www.embedgooglemap.net">embedgooglemap.net</a>
                    <style>
                        .gmap_canvas {
                            overflow: hidden;
                            background: none !important;
                            height: 400px;
                            width: 1000px;
                        }
                    </style>
                </div>
            </div>
        </div>
    </section>
    <!--================End Contact Area =================-->

    <!--================Footer Area =================-->
    <?php include "layout/footer.php"; ?>
    <!--================End Footer Area =================-->

</body>

</html>