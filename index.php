<!DOCTYPE html>
<html lang="en">

<?php include "layout/head.php"; ?>

<body class="home_full_width">

    <!--================Full Width Menu Area =================-->
    <?php include "layout/navigation_home.php"; ?>
    <!--================End Full Width Menu Area =================-->

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Gemunu+Libre:wght@400;700&family=Noto+Sans+Sinhala:wght@400;800&family=Stick+No+Bills:wght@400;600&display=swap" rel="stylesheet">

    <!--================Slider Area =================-->
    <section class="main_slider_area">
        <div id="fullwidth_slider" class="rev_slider" data-version="5.3.1.6" style="background-color: #76cfcf;">
            <ul>
                <li data-index="rs-1588" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="img/home-slider/slider-2.jpg" data-rotate="0"
                    data-saveperformance="off" data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="img/home-slider/full-width-2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="slider_text_box" >
                        <div class="tp-caption tp-resizeme first_text" data-x="['left','left','left','left','center','center']" data-hoffset="['30','30','10','0']" data-y="['top','top','top','top']" data-voffset="['250','250','250','250','160','160']" data-fontsize="['42','42','42','42','60','50']"
                            data-lineheight="['64','64','64','64','46','38']" data-width="['none','none','none','none','none']" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left','center','center']" style="font-family: 'Gemunu Libre', sans-serif; font-family: 'Noto Sans Sinhala', sans-serif; font-family: 'Stick No Bills', sans-serif;">
                            අපගේ සේවාවන්
                        </div>

                        <div class="tp-caption tp-resizeme third_text" data-x="['left','left','left','left','center','center']" data-hoffset="['30','30','30','0']" data-y="['top','top','top','top']" data-voffset="['340','340','340','340','240','200']" data-fontsize="['25','25','25','25','25']"
                            data-lineheight="['43','43','43','43']" data-width="['475','475','475','475','475','320']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-transform_idle="o:1;" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left','center','center']" style="font-family: 'Gemunu Libre', sans-serif; font-family: 'Noto Sans Sinhala', sans-serif; font-family: 'Stick No Bills', sans-serif;">
                            <B>
                                    <i class="arrow_right"></i>   උසස් තත්වයෙන් යුත් භාණ්ඩ​.
                                <br>
                                    <i class="arrow_right"></i>   අඛණ්ඩ විශ්වාසය. 
                                <br> 
                                    <i class="arrow_right"></i>   සුහදශීලී සේවය​.
                                <br> <i class="arrow_right"></i>   අලෙවියෙන් පසු සේවාව​.
                                <br> 
                                    <i class="arrow_right"></i>   වගකීමෙන් නිවසටම.
                            </B>
                        </div>

                        <div class="tp-caption tp-resizeme four_btn" data-x="['left','left','left','left','center','center']" data-hoffset="['30','30','30','0']" data-y="['top','top','top','top']" data-voffset="['575','575','575','575','400','400']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left','left','center']">
                            <a class="checkout_btn" href="product_categories.php">Shop Now</a>
                        </div>
                    </div>
                </li>
                <li data-index="rs-1587" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="img/home-slider/slider-1.jpg" data-rotate="0"
                    data-saveperformance="off" data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="img/home-slider/full-width-3.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>

                    <!-- LAYER NR. 1 -->
                    <div class="slider_text_box">


                        <div class="tp-caption tp-resizeme third_text" data-x="['left','left','left','left','center','center']" data-hoffset="['30','30','30','0']" data-y="['top','top','top','top']" data-voffset="['340','340','340','340','240','200']" data-fontsize="['25','25','25','25','25']"
                            data-lineheight="['43','43','43','43']" data-width="['475','475','475','475','475','320']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-transform_idle="o:1;" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left','center','center']" style="font-family: 'Gemunu Libre', sans-serif; font-family: 'Noto Sans Sinhala', sans-serif; font-family: 'Stick No Bills', sans-serif;">
                            <B>
                               සීට්ටු ක්‍රමයට <br> 
                               සියලුම වර්ගයේ ගෘහ භාණ්ඩ සහ <br> 
                               විදුලි උපකරණ වගකීමක් සහිතව <br> 
                               ලබාගැනීම සඳහා අප හා එක්වන්න.
                            </B>
                        </div>
                        <div class="tp-caption tp-resizeme four_btn" data-x="['left','left','left','left','center','center']" data-hoffset="['30','30','30','0']" data-y="['top','top','top','top']" data-voffset="['575','575','575','575','400','400']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left','left','center']">
                            <a class="checkout_btn" href="product_categories.php">Shop Now</a>
                        </div>
                    </div>
                </li>

                <li data-index="rs-1589" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="img/home-slider/slider-1.jpg" data-rotate="0"
                    data-saveperformance="off" data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="img/home-slider/full-width-1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>

                    <!-- LAYER NR. 1 -->
                    <div class="slider_text_box">
                        <div class="tp-caption tp-resizeme third_text" data-x="['left','left','left','left','center','center']" data-hoffset="['30','30','30','0']" data-y="['top','top','top','top']" data-voffset="['340','340','340','340','240','200']" data-fontsize="['25','25','25','25','25']"
                            data-lineheight="['43','43','43','43']" data-width="['475','475','475','475','475','320']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-transform_idle="o:1;" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left','center','center']" style="font-family: 'Gemunu Libre', sans-serif; font-family: 'Noto Sans Sinhala', sans-serif; font-family: 'Stick No Bills', sans-serif;">
                            <B>
                                භාණ්ඩවල සීග්‍ර <br> 
                                මිල වැඩිවීම් වලට සාපේක්‍ෂව <br> 
                                සීට්ටු සඳහා අඩුම මිල අපෙන්... <br>
                                දැන්ම අමතන්න​
                            </B>
                        </div>

                        <div class="tp-caption tp-resizeme four_btn" data-x="['left','left','left','left','center','center']" data-hoffset="['30','30','0','0']" data-y="['top','top','top','top']" data-voffset="['575','575','575','575','400','400']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                            data-textAlign="['left','left','left','left','left','center']">
                            <a class="checkout_btn" href="product_categories.php">Shop Now</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <!--================End Slider Area =================-->

    <!--================Feature Add Area =================-->
    <section class="feature_big_add_area">
        <div >
            <div class="row">
                <div class="col-lg-3">
                    <div class="f_add_item white_add">
                        <div class="f_add_img"><img class="img-fluid" src="image/banner/banner-right.jpg" alt=""></div>
                        <div class="f_add_hover">
                            <h4>Best Summer <br />Collection</h4>
                            <a class="add_btn" href="#">Shop Now <i class="arrow_right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="f_add_item white_add">
                        <div class="f_add_img"><img class="img-fluid" src="image/banner/banner-left.jpg" alt=""></div>
                        <div class="f_add_hover">
                            <h4>Best Summer <br />Collection</h4>
                            <a class="add_btn" href="#">Shop Now <i class="arrow_right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="f_add_item white_add">
                        <div class="f_add_img"><img class="img-fluid" src="image/banner/banner-right.jpg" alt=""></div>
                        <div class="f_add_hover">
                            <h4>Best Summer <br />Collection</h4>
                            <a class="add_btn" href="#">Shop Now <i class="arrow_right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="f_add_item white_add">
                        <div class="f_add_img"><img class="img-fluid" src="image/banner/banner-left.jpg" alt=""></div>
                        <div class="f_add_hover">
                            <h4>Best Summer <br />Collection</h4>
                            <a class="add_btn" href="#">Shop Now <i class="arrow_right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Feature Add Area =================-->

    <!-- Start Latest Product Area -->
    <div class="latest_product_3steps">
        <div class="s_m_title">
            <h2>Our Latest Product</h2>
        </div>
        <div class="l_product_slider owl-carousel">
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-1.jpg" alt="">
                        <h5 class="sale">Sale</h5>
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li style="font-size: 18px;">3750 x 12 = 45000</li>
                        </ul>
                        <br>
                        <h4>Coffee Table</h4>
                        <h5>(820)</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-2.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li style="font-size: 18px;">3750 x 12 = 45000</li>
                        </ul>
                        <br>
                        <h4>Blue Sofa</h4>
                        <h5>(821)</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-3.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li style="font-size: 18px;">3750 x 12 = 45000</li>
                        </ul>
                        <br>
                        <h4>Steel Chair</h4>
                        <h5>(822)</h5>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-4.jpg" alt="">
                        <h5 class="new">New</h5>
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li style="font-size: 18px;">3750 x 12 = 45000</li>
                        </ul>
                        <br>
                        <h4>Table Lamp</h4>
                        <h5>(823)</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-5.jpg" alt="">
                        <h5 class="sale">Sale</h5>
                    </div>
                    <div class="l_p_text">
                       <ul>
                            <li style="font-size: 18px;">3750 x 12 = 45000</li>
                        </ul>
                        <br>
                        <h4>Table Lamp</h4>
                        <h5>(824)</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-6.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li style="font-size: 18px;">3750 x 12 = 45000</li>
                        </ul>
                        <br>
                        <h4>Wood Chair</h4>
                        <h5>(825)</h5>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-7.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                    <ul>
                            <li style="font-size: 18px;">3750 x 12 = 45000</li>
                        </ul>
                        <br>
                        <h4>Wood Cabinet</h4>
                        <h5>(826)</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-8.jpg" alt="">
                        <h5 class="sale">Sale</h5>
                    </div>
                    <div class="l_p_text">
                    <ul>
                            <li style="font-size: 18px;">4250 x 12 = 51000</li>
                        </ul>
                        <br>
                        <h4>Wood Table</h4>
                        <h5>(833)</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-9.jpg" alt="">
                        <h5 class="sale">Sale</h5>
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li style="font-size: 18px;">4000 x 12 = 48000</li>
                        </ul>
                        <br>
                        <h4>Sofa Set</h4>
                        <h5>(834)</h5>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-10.jpg" alt="">
                        <h5 class="sale">Sale</h5>
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li style="font-size: 18px;">3750 x 12 = 45000</li>
                        </ul>
                        <br>
                        <h4>Double Sofa</h4>
                        <h5>(820)</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-11.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li style="font-size: 18px;">3750 x 12 = 45000</li>
                        </ul>
                        <br>
                        <h4>Wood Sofa Set</h4>
                        <h5>(820)</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img src="img/product/l-product-12.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li style="font-size: 18px;">3750 x 12 = 45000</li>
                        </ul>
                        <br>
                        <h4>Wood Chair</h4>
                        <h5>(820)</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Latest Product Area -->

    <!-- Start Our Service Area -->
    <section class="world_service">
        <div class="container">
            <div class="world_service_inner">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="world_service_item">
                            <h4><img src="img/icon/world-icon-1.png" alt="">Reliable Service</h4>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="world_service_item">
                            <h4><img src="img/icon/world-icon-2.png" alt="">After Service</h4>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="world_service_item">
                            <h4><img src="img/icon/world-icon-3.png" alt="">Quality Products</h4>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="world_service_item">
                            <h4><img src="img/icon/world-icon-4.png" alt="">Doorstep Delivery</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Service Area -->

    <!--================Brands =================-->
    <section class="from_blog_area">
        <div class="container">
            <div class="from_blog_inner">
                <div class="c_main_title">
                    <h2>Popular Brands</h2>
                </div>
                <div class="row">
                    <div class="col-sm-2 align-items">
                        <div class="">
                            <img class="img-fluid" src="image/arpico.jpg" alt="">

                        </div>
                    </div>
                    <div class="col-sm-2 align-items">
                        <div class="">
                            <img class="img-fluid" src="image/damro.jpg" alt="">

                        </div>
                    </div>
                    <div class="col-sm-2 align-items">
                        <div class="">
                            <img class="img-fluid" src="image/piyestra.png" alt="">

                        </div>
                    </div>
                    <div class="col-sm-2 align-items">
                        <div class="">
                            <img class="img-fluid" src="image/innovex.png" alt="">

                        </div>
                    </div>
                    <div class="col-sm-2 align-items">
                        <div class="">
                            <img class="img-fluid" style="max-width: 82px;" src="image/national.png" alt="">

                        </div>
                    </div>
                    <div class="col-sm-2 align-items">
                        <div class="">
                            <img class="img-fluid" src="image/nilkamal.png" alt="">

                        </div>
                    </div>
                    <!-- <div class="col-sm-2 align-items">
                        <div class="">
                            <img class="img-fluid" src="image/richsonic.png" alt="">

                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!--================Brands end=================-->

    <!--================Footer Area =================-->
    <?php include "layout/footer.php"; ?>
    <!--================End Footer Area =================-->
</body>

</html>