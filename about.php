<!DOCTYPE html>
<html lang="en">

<?php include "layout/head.php"; ?>
<!--================End Menu Area =================-->
<body>

<?php include "layout/navigation.php"; ?>
<!--================Categories Banner Area =================-->
<section class="categories_banner_area">
    <div class="container">
        <div class="solid_banner_inner">
            <h3>About Us</h3>
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="#">About Us</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Categories Banner Area =================-->

<!--================Contact Area =================-->
<section class="contact_area p_100">
    <div class="container">
        <div class="contact_title" style="text-align: left;">
            <h2 style="">ABOUT COMPANY PAGE</h2>
            <p style="margin: 0px;">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui.</p>
        </div>

        <div>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </p>
        </div>
    </div>
</section>
<!--================End Contact Area =================-->

<!--================Footer Area =================-->
<?php include "layout/footer.php"; ?>
<!--================End Footer Area =================-->
</body>

</html>