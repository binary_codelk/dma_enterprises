<!DOCTYPE html>
<html lang="en">

<?php include "layout/head.php"; ?>
<!--================End Menu Area =================-->

<body>

    <?php include "layout/navigation.php"; ?>
    <!--================Categories Banner Area =================-->
    <section class="categories_banner_area">
        <div class="container">
            <div class="solid_banner_inner">
                <h3>Terms and Conditions</h3>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="contact.html">Terms and Conditions</a></li>
                </ul>
            </div>
        </div>
    </section>
    <!--================End Categories Banner Area =================-->

    <!--================Terms Area =================-->
    <section class="contact_area p_100">
        <div class="container">
            <div class="contact_title" style="text-align: left;">
                <h1>Terms and Conditions</h1>
                <br>
                <ol>
                <li style="margin: 0px; font-size: 20px; padding:10px;">සියලුම විදුලි උපකරන සහ ලී භාණ්ඩ සඳහා වසරක වගකීමක් ඇත.</li>
                <li style="margin: 0px; font-size: 20px; padding:10px;">ලොව පිළිගත් සමාගම් වල භාණ්ඩ උසස් තත්වයෙන් ඔබට ලැබෙනවා දැයි පරික්‍ෂා කර භාණ්ඩ භාරගන්න​.</li>
                <li style="margin: 0px; font-size: 20px; padding:10px;">භාරදෙන භාණ්ඩයක දෝෂයක් ඇතොත් එදිනම අප ආයතනයට දැනුම් දෙන්න (පසු පැමිණිලි භාර නොගැනේ).</li>
                <li style="margin: 0px; font-size: 20px; padding:10px;">භාරදුන් භාණ්ඩ වල දෝෂ නොමැතිව නැවත මාරු කරණු නොලැබේ.</li>
                <li style="margin: 0px; font-size: 20px; padding:10px;">ලී බඩු සහ සෝෆා ඇනවුම් සඳහා සතියකට පෙර දැණුම් දෙන්න</li>
                <li style="margin: 0px; font-size: 20px; padding:10px;">කිසිදු අවස්ථාවක සාමාජිකයන් හට භාණ්ඩ හැර මුදල් ලබා දීමක් සිදු නොකෙරේ.</li>
                <li style="margin: 0px; font-size: 20px; padding:10px;">ආසන්න කණ්ඩ 04ක සීට්ටු කළවම් ලෙස එකතු කළ හැක. උද​: 1,2,3,4/ 7,8,9,10 යනාදී වශයෙනි.​​</li>
                </ol>
            </div>

            <div>
                <p style="font-size: 18px;">
                    රටේ පවතින කොරෝනා වසංගත තත්වය හේතුවෙන් ආනයනය කරනු ලබන භාණ්ඩයක් නොමැති වුවහොත් ඒ වෙනුවට වෙනත් භාණ්ඩයක් තෝරාගත යුතුවේ. කිසිඳු අවසිතාවක භාණ්ඩ හැර මුදල් ලබාදීමක් සිදු නොකෙරේ.​​
                </p>
            </div>
        </div>
    </section>
    <!--================End Contact Area =================-->

    <!--================Footer Area =================-->
    <?php include "layout/footer.php"; ?>
    <!--================End Footer Area =================-->
</body>

</html>