<header class="shop_header_area fullwidth_menu">
    <div class="carousel_menu_inner">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#"><img src="img/logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>

            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown submenu active">
                        <a class="nav-link dropdown-toggle" href="index.html" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Home </i>
                        </a>

                    </li>
                    <li class="nav-item"><a class="nav-link" href="product_categories.php">Products</a></li>
                    <li class="nav-item"><a class="nav-link" href="seetu_categories.php">Seetu Items</a></li>
                    <li class="nav-item"><a class="nav-link" href="about.php">About Us</a></li>
                    <li class="nav-item"><a class="nav-link" href="contact.php">Contact</a></li>
                </ul>
                <ul class="navbar-nav justify-content-end">
                    <li class="search_icon"><a href="#"><i class="fa fa-phone"></i> Call Us:   +94715122228</a></li>
                   
                    
                </ul>
            </div>
        </nav>
    </div>
</header>