<!--================Menu Area =================-->
<header class="shop_header_area carousel_menu_area">
    <div class="align-items" style="justify-content: flex-end; margin-top: 10px;margin-right: 10px;
    margin-bottom: -11px;">
        <div class="col-sm-12 align-end">
            <div class="col-lg-8 col-sm-7 align-end">
                <a href="#" style="color: black; padding-right: 10px" class="call-option"><i class="fa fa-phone"></i> Call Us: <span>+94 715 122
                        228</span></a>
            </div>
            <div class="col-lg-4 col-sm-5 align-end">
                <a href="#" style="color: black;"><i class="fa fa-envelope"></i> Email:
                    <span>disssanaajith63@gmail.com</span></a>
            </div>
        </div>

    </div>
    <div class="carousel_menu_inner">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding:0px;    border-top:none;">
                <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>

                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent" style="align-content: flex-end;justify-content: flex-end;">
                    <ul class="navbar-nav ">
                        <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                        <li class="nav-item "><a class="nav-link" href="product_categories.php">Products</a></li>
                        <li class="nav-item "><a class="nav-link" href="seetu_categories.php">Seetu Items</a></li>
                        <li class="nav-item"><a class="nav-link" href="about.php">About Us</a></li>
                        <li class="nav-item "><a class="nav-link" href="contact.php">Contact</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>